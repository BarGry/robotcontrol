// GENERATED AUTOMATICALLY FROM 'Assets/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""4aac39fc-b07a-418a-a02f-a63642b6e6fc"",
            ""actions"": [
                {
                    ""name"": ""NextGripperState"",
                    ""type"": ""Button"",
                    ""id"": ""048a3882-25bd-474a-9d61-52d7dfbef848"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftStick"",
                    ""type"": ""Value"",
                    ""id"": ""8080b800-5526-42e2-bf7e-b706b1b079e9"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightStick"",
                    ""type"": ""Value"",
                    ""id"": ""691cdfdb-8342-41e8-ba8e-5edbfd47f3d4"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PreviousGripperState"",
                    ""type"": ""Button"",
                    ""id"": ""26c1c120-0de6-4871-8c0b-a4081c97e3ba"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""IncresseJogSpeed"",
                    ""type"": ""Button"",
                    ""id"": ""992339c8-f6a6-407a-b991-9b50fef43534"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DecresseJogSpeed"",
                    ""type"": ""Button"",
                    ""id"": ""d77d52a0-813b-420a-b843-5867aee78195"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""EnableJogMode"",
                    ""type"": ""Button"",
                    ""id"": ""d626c4e6-45cb-42db-8e19-b5fcb65a40ba"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RunFbMoveToPoint"",
                    ""type"": ""Button"",
                    ""id"": ""d28c0d34-e5b0-4574-806f-3bd8f8f9721a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RunFbPickAndPlace"",
                    ""type"": ""Button"",
                    ""id"": ""5cbbd72a-2c6a-437c-9970-e5772e4341ad"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DisableJogMode"",
                    ""type"": ""Button"",
                    ""id"": ""231f1ab0-17c2-4bd8-a030-94dc9e7e8aaf"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8aa0f34f-c109-4d07-ad0d-3a0a2ae07542"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextGripperState"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d6ec41c9-e106-48c4-a92d-878ef4469b1c"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""04e2ead2-24d1-4e1e-8a7e-7377ff677c36"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3bece618-1276-4dd6-a332-ecf8e9b85915"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PreviousGripperState"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85e5d95b-b14e-4551-8885-b32cfe4a485d"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""IncresseJogSpeed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2db8938d-ba51-4d26-bc79-2bf9e0a711b1"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DecresseJogSpeed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af68d352-0fcf-4b39-94b4-ca51fc4d35ac"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EnableJogMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c7865747-2893-45ff-a9cb-e1662716b59c"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RunFbMoveToPoint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85614a47-e557-4c03-aea1-eb412e0becae"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RunFbPickAndPlace"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""34b1b745-00b3-4ed2-8a24-1a4b496ad0ee"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DisableJogMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_NextGripperState = m_Gameplay.FindAction("NextGripperState", throwIfNotFound: true);
        m_Gameplay_LeftStick = m_Gameplay.FindAction("LeftStick", throwIfNotFound: true);
        m_Gameplay_RightStick = m_Gameplay.FindAction("RightStick", throwIfNotFound: true);
        m_Gameplay_PreviousGripperState = m_Gameplay.FindAction("PreviousGripperState", throwIfNotFound: true);
        m_Gameplay_IncresseJogSpeed = m_Gameplay.FindAction("IncresseJogSpeed", throwIfNotFound: true);
        m_Gameplay_DecresseJogSpeed = m_Gameplay.FindAction("DecresseJogSpeed", throwIfNotFound: true);
        m_Gameplay_EnableJogMode = m_Gameplay.FindAction("EnableJogMode", throwIfNotFound: true);
        m_Gameplay_RunFbMoveToPoint = m_Gameplay.FindAction("RunFbMoveToPoint", throwIfNotFound: true);
        m_Gameplay_RunFbPickAndPlace = m_Gameplay.FindAction("RunFbPickAndPlace", throwIfNotFound: true);
        m_Gameplay_DisableJogMode = m_Gameplay.FindAction("DisableJogMode", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_NextGripperState;
    private readonly InputAction m_Gameplay_LeftStick;
    private readonly InputAction m_Gameplay_RightStick;
    private readonly InputAction m_Gameplay_PreviousGripperState;
    private readonly InputAction m_Gameplay_IncresseJogSpeed;
    private readonly InputAction m_Gameplay_DecresseJogSpeed;
    private readonly InputAction m_Gameplay_EnableJogMode;
    private readonly InputAction m_Gameplay_RunFbMoveToPoint;
    private readonly InputAction m_Gameplay_RunFbPickAndPlace;
    private readonly InputAction m_Gameplay_DisableJogMode;
    public struct GameplayActions
    {
        private @PlayerControls m_Wrapper;
        public GameplayActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @NextGripperState => m_Wrapper.m_Gameplay_NextGripperState;
        public InputAction @LeftStick => m_Wrapper.m_Gameplay_LeftStick;
        public InputAction @RightStick => m_Wrapper.m_Gameplay_RightStick;
        public InputAction @PreviousGripperState => m_Wrapper.m_Gameplay_PreviousGripperState;
        public InputAction @IncresseJogSpeed => m_Wrapper.m_Gameplay_IncresseJogSpeed;
        public InputAction @DecresseJogSpeed => m_Wrapper.m_Gameplay_DecresseJogSpeed;
        public InputAction @EnableJogMode => m_Wrapper.m_Gameplay_EnableJogMode;
        public InputAction @RunFbMoveToPoint => m_Wrapper.m_Gameplay_RunFbMoveToPoint;
        public InputAction @RunFbPickAndPlace => m_Wrapper.m_Gameplay_RunFbPickAndPlace;
        public InputAction @DisableJogMode => m_Wrapper.m_Gameplay_DisableJogMode;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @NextGripperState.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextGripperState;
                @NextGripperState.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextGripperState;
                @NextGripperState.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnNextGripperState;
                @LeftStick.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnLeftStick;
                @LeftStick.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnLeftStick;
                @LeftStick.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnLeftStick;
                @RightStick.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRightStick;
                @RightStick.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRightStick;
                @RightStick.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRightStick;
                @PreviousGripperState.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousGripperState;
                @PreviousGripperState.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousGripperState;
                @PreviousGripperState.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPreviousGripperState;
                @IncresseJogSpeed.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnIncresseJogSpeed;
                @IncresseJogSpeed.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnIncresseJogSpeed;
                @IncresseJogSpeed.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnIncresseJogSpeed;
                @DecresseJogSpeed.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDecresseJogSpeed;
                @DecresseJogSpeed.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDecresseJogSpeed;
                @DecresseJogSpeed.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDecresseJogSpeed;
                @EnableJogMode.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEnableJogMode;
                @EnableJogMode.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEnableJogMode;
                @EnableJogMode.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEnableJogMode;
                @RunFbMoveToPoint.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbMoveToPoint;
                @RunFbMoveToPoint.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbMoveToPoint;
                @RunFbMoveToPoint.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbMoveToPoint;
                @RunFbPickAndPlace.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbPickAndPlace;
                @RunFbPickAndPlace.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbPickAndPlace;
                @RunFbPickAndPlace.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRunFbPickAndPlace;
                @DisableJogMode.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDisableJogMode;
                @DisableJogMode.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDisableJogMode;
                @DisableJogMode.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDisableJogMode;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @NextGripperState.started += instance.OnNextGripperState;
                @NextGripperState.performed += instance.OnNextGripperState;
                @NextGripperState.canceled += instance.OnNextGripperState;
                @LeftStick.started += instance.OnLeftStick;
                @LeftStick.performed += instance.OnLeftStick;
                @LeftStick.canceled += instance.OnLeftStick;
                @RightStick.started += instance.OnRightStick;
                @RightStick.performed += instance.OnRightStick;
                @RightStick.canceled += instance.OnRightStick;
                @PreviousGripperState.started += instance.OnPreviousGripperState;
                @PreviousGripperState.performed += instance.OnPreviousGripperState;
                @PreviousGripperState.canceled += instance.OnPreviousGripperState;
                @IncresseJogSpeed.started += instance.OnIncresseJogSpeed;
                @IncresseJogSpeed.performed += instance.OnIncresseJogSpeed;
                @IncresseJogSpeed.canceled += instance.OnIncresseJogSpeed;
                @DecresseJogSpeed.started += instance.OnDecresseJogSpeed;
                @DecresseJogSpeed.performed += instance.OnDecresseJogSpeed;
                @DecresseJogSpeed.canceled += instance.OnDecresseJogSpeed;
                @EnableJogMode.started += instance.OnEnableJogMode;
                @EnableJogMode.performed += instance.OnEnableJogMode;
                @EnableJogMode.canceled += instance.OnEnableJogMode;
                @RunFbMoveToPoint.started += instance.OnRunFbMoveToPoint;
                @RunFbMoveToPoint.performed += instance.OnRunFbMoveToPoint;
                @RunFbMoveToPoint.canceled += instance.OnRunFbMoveToPoint;
                @RunFbPickAndPlace.started += instance.OnRunFbPickAndPlace;
                @RunFbPickAndPlace.performed += instance.OnRunFbPickAndPlace;
                @RunFbPickAndPlace.canceled += instance.OnRunFbPickAndPlace;
                @DisableJogMode.started += instance.OnDisableJogMode;
                @DisableJogMode.performed += instance.OnDisableJogMode;
                @DisableJogMode.canceled += instance.OnDisableJogMode;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnNextGripperState(InputAction.CallbackContext context);
        void OnLeftStick(InputAction.CallbackContext context);
        void OnRightStick(InputAction.CallbackContext context);
        void OnPreviousGripperState(InputAction.CallbackContext context);
        void OnIncresseJogSpeed(InputAction.CallbackContext context);
        void OnDecresseJogSpeed(InputAction.CallbackContext context);
        void OnEnableJogMode(InputAction.CallbackContext context);
        void OnRunFbMoveToPoint(InputAction.CallbackContext context);
        void OnRunFbPickAndPlace(InputAction.CallbackContext context);
        void OnDisableJogMode(InputAction.CallbackContext context);
    }
}
