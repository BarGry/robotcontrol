using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem.DualShock;

public class GamePadValue : MonoBehaviour
{
    PlayerControls controls;

    [SerializeField] TextMeshProUGUI gripperStateInfo;
    [SerializeField] TextMeshProUGUI toggleIsConnectedControllerInof;
    [SerializeField] TextMeshProUGUI leftStickData, rightStickData;
    [SerializeField] TextMeshProUGUI positionAreaStateStatus;
    [SerializeField] Image leftAnalogImage, rightAnalogImage;
    [SerializeField] TextMeshProUGUI infoSetGreen;
    [SerializeField] Slider wibrationSpeedDataInput;
    [SerializeField] TextMeshProUGUI wibrationSpeedDataOutput;
    private int numberOfSetColor;
    public short gripperState = 0;
    public Vector2 leftStickVector2d, rightStickVector2d;
    public bool jogSpeedInc, jogSpeedDec, enableJogMotionMode, disableJogMotionMode, performFbMoveToPoint, performFbPickAndPlace;
    float wibrationSpeed;
    private void Awake()
    {
        controls = new PlayerControls();
        controls.Gameplay.NextGripperState.performed += ctx => NextGripperState();
        controls.Gameplay.PreviousGripperState.performed += ctx => PreviousGripperState();
        controls.Gameplay.LeftStick.performed += ctx => leftStickVector2d = ctx.ReadValue<Vector2>();
        controls.Gameplay.LeftStick.canceled += ctx => leftStickVector2d = Vector2.zero;
        controls.Gameplay.RightStick.performed += ctx => rightStickVector2d = ctx.ReadValue<Vector2>();
        controls.Gameplay.RightStick.canceled += ctx => rightStickVector2d = Vector2.zero;
        controls.Gameplay.IncresseJogSpeed.performed += ctx => IncresseJogSpeedSet();
        controls.Gameplay.IncresseJogSpeed.canceled += ctx => IncresseJogSpeedReset();
        controls.Gameplay.DecresseJogSpeed.performed += ctx => DecresseJogSpeedSet();
        controls.Gameplay.DecresseJogSpeed.canceled += ctx => DecresseJogSpeedReset();
        controls.Gameplay.EnableJogMode.performed += ctx => EnableJogModeSet();
        controls.Gameplay.EnableJogMode.canceled += ctx => EnableJogModeReset();
        controls.Gameplay.DisableJogMode.performed += ctx => DisableJogModeSet();
        controls.Gameplay.DisableJogMode.canceled += ctx => DisableJogModeReset();
        controls.Gameplay.RunFbMoveToPoint.performed += ctx => RunFbMoveToPointSet();
        controls.Gameplay.RunFbMoveToPoint.canceled += ctx => RunFbMoveToPointReset();
        controls.Gameplay.RunFbPickAndPlace.performed += ctx => RunFbPickAndPlaceSet();
        controls.Gameplay.RunFbPickAndPlace.canceled += ctx => RunFbPickAndPlaceReset();
    }
    SlmpMenager slmpMangaer;
    SlmpMenager.AllReadedData allReadedData;
    private void Start()
    {
        slmpMangaer = GameObject.Find("AllScriptsHandler").GetComponent<SlmpMenager>();
        allReadedData = new SlmpMenager.AllReadedData();
        wibrationSpeed = wibrationSpeedDataInput.value;
        wibrationSpeedDataOutput.text = wibrationSpeed.ToString();
    }
    private byte areaStateAll = 0, areaStateX = 0, areaStateY = 0, areaStateZ = 0, areaStateC = 0;
    private string areaStateAllDebug, areaStateXDebug, areaStateYDebug, areaStateZDebug, areaStateCDebug;
    float machineAreaLengthX, machineAreaLengthY, machineAreaLengthZ;
    double machineAreaLengthC;
    float machineAreaCautPosXMin, machineAreaCautPosYMin, machineAreaCautPosZMin;
    float machineAreaCautPosXMax, machineAreaCautPosYMax, machineAreaCautPosZMax;
    float machineAreaWarnPosXMin, machineAreaWarnPosYMin, machineAreaWarnPosZMin;
    float machineAreaWarnPosXMax, machineAreaWarnPosYMax, machineAreaWarnPosZMax;
    double machineAreaCautAngleCMin, machineAreaCautAngleCMax, machineAreaWarnAngleCMin, machineAreaWarnAngleCMax;
    float warnMargin = 0.05f;
    float cautMargin = 0.15f;
    private bool isWarning, isCaution;

    private void Update()
    {
        wibrationSpeed = wibrationSpeedDataInput.value;
        wibrationSpeedDataOutput.text = wibrationSpeed.ToString();
        if (Gamepad.current != null)
        {            
            toggleIsConnectedControllerInof.text = "Connected [" + Gamepad.current.device.name + "]";
            leftStickData.text = "X: " + leftStickVector2d.x.ToString() + "\nY: " + leftStickVector2d.y.ToString();
            leftAnalogImage.transform.localPosition = 50*leftStickVector2d;
            rightStickData.text = "X: " + rightStickVector2d.x.ToString() + "\nY: " + rightStickVector2d.y.ToString();
            rightAnalogImage.transform.localPosition = 50 * rightStickVector2d;
            gripperStateInfo.text = gripperState.ToString();
            allReadedData = slmpMangaer.GetAllReadedData();
            if (allReadedData.correctReaded)
            {
                machineAreaLengthX = allReadedData.machineLimitMax.x - allReadedData.machineLimitMin.x;
                machineAreaLengthY = allReadedData.machineLimitMax.y - allReadedData.machineLimitMin.y;
                machineAreaLengthZ = allReadedData.machineLimitMax.z - allReadedData.machineLimitMin.z;
                machineAreaLengthC = allReadedData.machineLimitAngleCMax - allReadedData.machineLimitAngleCMin;

                machineAreaCautPosXMin = allReadedData.machineLimitMin.x + (machineAreaLengthX * cautMargin);
                machineAreaCautPosYMin = allReadedData.machineLimitMin.y + (machineAreaLengthY * cautMargin);
                machineAreaCautPosZMin = allReadedData.machineLimitMin.z + (machineAreaLengthZ * cautMargin);
                machineAreaCautAngleCMin = allReadedData.machineLimitAngleCMin + (machineAreaLengthC * cautMargin);

                machineAreaCautPosXMax = allReadedData.machineLimitMax.x - (machineAreaLengthX * cautMargin);
                machineAreaCautPosYMax = allReadedData.machineLimitMax.y - (machineAreaLengthY * cautMargin);
                machineAreaCautPosZMax = allReadedData.machineLimitMax.z - (machineAreaLengthZ * cautMargin);
                machineAreaCautAngleCMax = allReadedData.machineLimitAngleCMax - (machineAreaLengthC * cautMargin);

                machineAreaWarnPosXMin = allReadedData.machineLimitMin.x + (machineAreaLengthX * warnMargin);
                machineAreaWarnPosYMin = allReadedData.machineLimitMin.y + (machineAreaLengthY * warnMargin);
                machineAreaWarnPosZMin = allReadedData.machineLimitMin.z + (machineAreaLengthZ * warnMargin);
                machineAreaWarnAngleCMin = allReadedData.machineLimitAngleCMin + (machineAreaLengthC * warnMargin);

                machineAreaWarnPosXMax = allReadedData.machineLimitMax.x - (machineAreaLengthX * warnMargin);
                machineAreaWarnPosYMax = allReadedData.machineLimitMax.y - (machineAreaLengthY * warnMargin);
                machineAreaWarnPosZMax = allReadedData.machineLimitMax.z - (machineAreaLengthZ * warnMargin);
                machineAreaWarnAngleCMax = allReadedData.machineLimitAngleCMax - (machineAreaLengthC * warnMargin);
                
                isWarning = false; isCaution = false;

                if (machineAreaCautPosXMin < allReadedData.currentPosition.x && machineAreaCautPosXMax > allReadedData.currentPosition.x)
                {
                    areaStateX = 0;
                    areaStateXDebug = "X: " + areaStateX.ToString() + " is OK";
                }
                else if(machineAreaCautPosXMin >= allReadedData.currentPosition.x && machineAreaWarnPosXMin < allReadedData.currentPosition.x)
                {
                    areaStateX = 11;
                    areaStateXDebug = "X: " + areaStateX.ToString() + " is Caut Min";
                    isCaution = true;
                }
                else if (machineAreaWarnPosXMin >= allReadedData.currentPosition.x)
                {
                    areaStateX = 12;
                    areaStateXDebug = "X: " + areaStateX.ToString() + " is Warn Min";
                    isWarning = true;
                }
                else if (machineAreaCautPosXMax <= allReadedData.currentPosition.x && machineAreaWarnPosXMax > allReadedData.currentPosition.x)
                {
                    areaStateX = 21;
                    areaStateXDebug = "X: " + areaStateX.ToString() + " is Caut Max";
                    isCaution = true;
                }
                else if (machineAreaWarnPosXMax <= allReadedData.currentPosition.x)
                {
                    areaStateX = 22;
                    areaStateXDebug = "X: " + areaStateX.ToString() + " is Warn Max";
                    isWarning = true;
                }

                if (machineAreaCautPosYMin < allReadedData.currentPosition.y && machineAreaCautPosYMax > allReadedData.currentPosition.y)
                {
                    areaStateY = 0;
                    areaStateYDebug = "Y: " + areaStateY.ToString() + " is OK";
                }
                else if (machineAreaCautPosYMin >= allReadedData.currentPosition.y && machineAreaWarnPosYMin < allReadedData.currentPosition.y)
                {
                    areaStateY = 11;
                    areaStateYDebug = "Y: " + areaStateY.ToString() + " is Caut Min";
                    isCaution = true;
                }
                else if (machineAreaWarnPosYMin >= allReadedData.currentPosition.y)
                {
                    areaStateY = 12;
                    areaStateYDebug = "Y: " + areaStateY.ToString() + " is Warn Min";
                    isWarning = true;
                }
                else if (machineAreaCautPosYMax <= allReadedData.currentPosition.y && machineAreaWarnPosYMax > allReadedData.currentPosition.y)
                {
                    areaStateY = 21;
                    areaStateYDebug = "Y: " + areaStateY.ToString() + " is Caut Max";
                    isCaution = true;
                }
                else if (machineAreaWarnPosYMax <= allReadedData.currentPosition.y)
                {
                    areaStateY = 22;
                    areaStateYDebug = "Y: " + areaStateY.ToString() + " is Warn Max";
                    isWarning = true;
                }

                if (machineAreaCautPosZMin < allReadedData.currentPosition.z && machineAreaCautPosZMax > allReadedData.currentPosition.z)
                {
                    areaStateZ = 0;
                    areaStateZDebug = "Z: " + areaStateZ.ToString() + " is OK";
                }
                else if (machineAreaCautPosZMin >= allReadedData.currentPosition.z && machineAreaWarnPosZMin < allReadedData.currentPosition.z)
                {
                    areaStateZ = 11;
                    areaStateZDebug = "Z: " + areaStateZ.ToString() + " is Caut Min";
                    isCaution = true;
                }
                else if (machineAreaWarnPosZMin >= allReadedData.currentPosition.z)
                {
                    areaStateZ = 12;
                    areaStateZDebug = "Z: " + areaStateZ.ToString() + " is Warn Min";
                    isWarning = true;
                }
                else if (machineAreaCautPosZMax <= allReadedData.currentPosition.z && machineAreaWarnPosZMax > allReadedData.currentPosition.z)
                {
                    areaStateZ = 21;
                    areaStateZDebug = "Z: " + areaStateZ.ToString() + " is Caut Max";
                    isCaution = true;
                }
                else if (machineAreaWarnPosZMax <= allReadedData.currentPosition.z)
                {
                    areaStateZ = 22;
                    areaStateZDebug = "Z: " + areaStateZ.ToString() + " is Warn Max";
                    isWarning = true;
                }

                if (machineAreaCautAngleCMin < allReadedData.currentAngleC && machineAreaCautAngleCMax > allReadedData.currentAngleC)
                {
                    areaStateC = 0;
                    areaStateCDebug = "C: " + areaStateC.ToString() + " is OK";
                }
                else if (machineAreaCautAngleCMin >= allReadedData.currentAngleC && machineAreaWarnAngleCMin < allReadedData.currentAngleC)
                {
                    areaStateC = 11;
                    areaStateCDebug = "C: " + areaStateC.ToString() + " is Caut Min";
                    isCaution = true;
                }
                else if (machineAreaWarnAngleCMin >= allReadedData.currentAngleC)
                {
                    areaStateC = 12;
                    areaStateCDebug = "C: " + areaStateC.ToString() + " is Warn Min";
                    isWarning = true;
                }
                else if (machineAreaCautAngleCMax <= allReadedData.currentAngleC && machineAreaWarnAngleCMax > allReadedData.currentAngleC)
                {
                    areaStateC = 21;
                    areaStateCDebug = "C: " + areaStateC.ToString() + " is Caut Max";
                    isCaution = true;
                }
                else if (machineAreaWarnAngleCMax <= allReadedData.currentAngleC)
                {
                    areaStateC = 22;
                    areaStateCDebug = "C: " + areaStateC.ToString() + " is Warn Max";
                    isWarning = true;
                }

                if (isWarning)
                {
                    areaStateAll = 2;
                    areaStateAllDebug = "All is Warning!!!";
                }
                else if(isCaution)
                {
                    areaStateAll = 1;
                    areaStateAllDebug = "All is Caution!";
                }
                else
                {
                    areaStateAll = 0;
                    areaStateAllDebug = "All is OK";
                }

                positionAreaStateStatus.text = areaStateAllDebug + "\n" + areaStateXDebug + "\n" + areaStateYDebug + "\n" + areaStateZDebug + "\n" + areaStateCDebug;

                if (isWarning)
                {
                    timeRemaining -= Time.deltaTime;
                    if (timeRemaining <= 0)
                    {
                        if (wibrationState == false)
                        {
                            Gamepad.current.SetMotorSpeeds(wibrationSpeed, wibrationSpeed);
                            timeRemaining = timeThickWibration;
                            wibrationState = true;
                        }
                        else
                        {
                            Gamepad.current.SetMotorSpeeds(0.0f, 0.0f);
                            timeRemaining = timeThickNoWibration;
                            wibrationState = false;
                        }
                    }
                    ChangeToColorState(3);
                    //if (currentColorChange != 3)
                    //{
                    //    ChangeToColorState(3);
                    //}
                }
                else if (isCaution)
                {
                    timeRemaining -= Time.deltaTime;
                    if (timeRemaining <= 0)
                    {
                        if (wibrationState == false)
                        {
                            Gamepad.current.SetMotorSpeeds(0.0f, wibrationSpeed);
                            timeRemaining = timeThickWibration;
                            wibrationState = true;
                        }
                        else
                        {
                            Gamepad.current.SetMotorSpeeds(0.0f, 0.0f);
                            timeRemaining = timeThickNoWibration;
                            wibrationState = false;
                        }
                    }
                    ChangeToColorState(2);
                    //if (currentColorChange != 2)
                    //{
                    //    ChangeToColorState(2);
                    //}
                }
                else
                {
                    Gamepad.current.SetMotorSpeeds(0.0f, 0.0f);
                    timeRemaining = 0;
                    wibrationState = false;
                    ChangeToColorState(1);                    
                    //if (currentColorChange != 1)
                    //{
                    //    ChangeToColorState(1);
                    //}
                }
            }
        }
        else
        {
            toggleIsConnectedControllerInof.text = "Disconnected";
            leftStickVector2d = Vector2.zero;
            rightStickVector2d = Vector2.zero;
        }
    }
    byte currentColorChange = 0;
    private void ChangeToColorState(byte index)
    {
        if (Gamepad.current.name == "DualShock4GamepadHID")
        {
            var gampePad = (DualShockGamepad)Gamepad.current;
            switch (index)
            {
                case 1:
                    gampePad.SetLightBarColor(Color.green);
                    numberOfSetColor++;
                    infoSetGreen.text = "Set Green times: " + numberOfSetColor.ToString();
                    break;
                case 2:
                    gampePad.SetLightBarColor(Color.yellow);
                    numberOfSetColor++;
                    infoSetGreen.text = "Set Yellow times: " + numberOfSetColor.ToString();
                    break;
                case 3:
                    gampePad.SetLightBarColor(Color.red);
                    numberOfSetColor++;
                    infoSetGreen.text = "Set Red times: " + numberOfSetColor.ToString();
                    break;
            }            
        }
        currentColorChange = index;
    }
    float timeRemaining = 0.0f;
    float timeThickWibration = 1.0f, timeThickNoWibration = 0.5f;
    bool wibrationState = false;
    void NextGripperState()
    {
        gripperState++;
        if(gripperState > 2)
        {
            gripperState = 0;
        }        
    }
    void PreviousGripperState()
    {
        gripperState--;
        if (gripperState < 0)
        {
            gripperState = 2;
        }
    }
    void IncresseJogSpeedSet()
    {
        jogSpeedInc = true;
    }
    void IncresseJogSpeedReset()
    {
        jogSpeedInc = false;
    }
    void DecresseJogSpeedSet()
    {
        jogSpeedDec = true;
    }
    void DecresseJogSpeedReset()
    {
        jogSpeedDec = false;
    }
    void EnableJogModeSet()
    {
        enableJogMotionMode = true;
    }
    void EnableJogModeReset()
    {
        enableJogMotionMode = false;
    }
    void DisableJogModeSet()
    {
        disableJogMotionMode = true;
    }
    void DisableJogModeReset()
    {
        disableJogMotionMode = false;
    }
    void RunFbMoveToPointSet()
    {
        performFbMoveToPoint = true;
    }
    void RunFbMoveToPointReset()
    {
        performFbMoveToPoint = false;
    }
    void RunFbPickAndPlaceSet()
    {
        performFbPickAndPlace = true;
    }
    void RunFbPickAndPlaceReset()
    {
        performFbPickAndPlace = false;
    }
    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }
    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}